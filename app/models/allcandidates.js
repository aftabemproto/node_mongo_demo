var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var allcandidatesSchema = new Schema({

candidate_name:{type:String,require:true},
designation:{type:String,require:true},
experience:{type:Number},
email:{type:String},
mobileno:{type:String},
jobdescription:{type:String},


});

module.exports = mongoose.model('candidatestable',allcandidatesSchema);