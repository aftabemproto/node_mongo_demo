var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var RegisterSchema   = new Schema({

    name: {type:String,require:true,unique:false},
    password :{type:String,require:true},
    emailid:{type:String,require:true,unique:true},
    mobileno:{type:String,require:true,unique:true},
    designation:{type:String,require:true},
    experience:{type:Number,require},


});

module.exports = mongoose.model('allcontacts', RegisterSchema);
