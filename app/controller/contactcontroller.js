var Customers = require('../models/customers');

module.exports = {
  create: function (req, res) {
    var customers = Customers();
    console.log(req.body.addresses[0])
    customers.candidate_name = req.body.customername;
    customers.addresses = req.body.addresses;
    customers.save(function (err) {
      if (err)
        res.send(err)
      else
        res.send(JSON.stringify({ status: true, message: 'record added' }));
    })
  },
  get: (req, res) => {
    Customers.find(function (err, allCustomers) {
      if (err)
        res.send(err);
      res.send(JSON.stringify({ status: true, allCustomers }));
      console.log(allCustomers)
    })

  }
}

