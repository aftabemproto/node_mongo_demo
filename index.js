var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var router = express.Router();
var multer = require('multer');

var Contact = require('./app/models/register');
var Category = require('./app/models/category');
var Usertype = require('./app/models/usertype');
var Subject = require('./app/models/allsubject');
var Candidates = require('./app/models/allcandidates');
var OfferModel = require('./app/models/alloffer');
var Imagemodel = require('./app/models/imagedetails');
var Customers = require('./app/models/customers');
var ccontroller = require('./app/controller/contactcontroller');
var imageurl;

mongoose.Promise = global.Promise;
//app.use(express.bodyParser());
//app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'));

////image uploading////
// var Storage = multer.diskStorage({

// destination:function(req,file,callback){
// callback(null,"./Images");
// },
// filename:function(req,file,callback){
//   callback(null,file.fieldname+"_"+Date.now() +  "_" + file.originalname);
//   imageurl = "http://localhost:3000/Images/"+file.fieldname+"_"+Date.now() +  "_" + file.originalname;

// }

// });
// var upload = multer({storage:Storage}).array("imageUploader",3);
// var upload1 = multer({storage:Storage}).array("imageUploader",3);

//////

var uristring =
  process.env.MONGOLAB_URI ||
  process.env.MONGOHQ_URL ||
  'mongodb://localhost/lohaapisdatabase';

// The http server will listen to an appropriate port, or default to
// port 5000.
var theport = process.env.PORT || 3000;

// Makes connection asynchronously.  Mongoose will queue up database
// operations and release them when the connection is complete.
mongoose.connect(uristring, function (err, res) {
  if (err) {
    console.log('ERROR connecting to: ' + uristring + '. ' + err);

  } else {
    //console.log(ccontroller.create)
    console.log('Succeeded connected to: ' + uristring);
  }
});


/*router.get("/", function (req, res) {
    res.sendFile("/home/ptblr-1199/Documents/UploadFiles/JavaScript/Upload/Index.html");
 //res.sendFile( "/home/ptblr-1190/Documents/Node"+"/"+ "form.htm" );
});
*/

///////login api//////////////
router.route('/login').post(function (req, res) {
  var contact = new Contact();
  Contact.findOne({ mobileno: req.body.mobileno, password: req.body.password }, function (err, data) {

    if (err) {
      res.end(JSON.stringify(err));
    }
    else {
      if (data === null) {
        var error = { "Error": "username is not present" };
        res.send(JSON.stringify({ "status": false, message: "username and password is not present" }));

      }
      else {
        res.send(JSON.stringify({ status: true, data, message: 'login successfully' }));
      }

    }

  });

});
//creating new customer

router.route('/addCustomer').post(ccontroller.create).get(ccontroller.get)
router.route('/addAddress').post(function (req, res) {
  Customers.update({ candidate_name: req.body.customername }, { $push: { 'addresses': { street: req.body.street, pincode: req.body.pincode } } }, function (err) {
    if (err)
      res.send(err);
    else
      res.send(JSON.stringify({ status: true, message: 'address added successfully' }));
  })
})

//////offer api////////
router.route('/addoffer').post(function (req, res) {
  var addoffer = new OfferModel();
  addoffer.productname = req.body.productname;
  addoffer.productdescription = req.body.productdescription;
  addoffer.unit_name = req.body.unit_name;
  addoffer.product_specification = req.body.product_specification;
  addoffer.user_email_id = req.body.user_email_id;
  addoffer.user_phone = req.body.user_phone;
  addoffer.save(function (err) {
    if (err) {
      res.send(err);
    }
    else {
      res.send(JSON.stringify({ status: true, message: 'offer Added successfully' }));
    }

  })


})
  .get(function (req, res) {

    OfferModel.find(function (err, alloffer) {
      if (err)
        res.send(err);
      res.send(JSON.stringify({ status: true, alloffer }));

    });

  })


router.get('/', function (req, res) {
  res.json(jsonresponce);
})


/////////updating data in candidate database/////
router.route('/updatecandidate').post(function (req, res) {

  Candidates.updateMany({ candidate_name: req.body.candidatename }, { $set: { email: req.body.email } }, { upsert: true }, function (err) {
    if (err) {
      res.send(err);
    }
    else {
      res.send(JSON.stringify({ status: true, message: 'email updated successfully' }));
    }
  })

})

router.route('/addcandidate').post(function (req, res) {

  var candidate = new Candidates();
  candidate.candidate_name = req.body.candidatename;
  candidate.designation = req.body.designation;
  candidate.experience = req.body.experience;
  candidate.email = req.body.email;
  candidate.mobileno = req.body.mobilenumber;
  candidate.jobdescription = req.body.jobdescription;
  //console.log(req.query);
  console.log(req.body.candidatename);

  candidate.save(function (err) {
    if (err) {
      res.send(err);
    }
    else {
      res.send(JSON.stringify({ status: true, message: 'candidate Added successfully' }));
    }

  })


})
  .get(function (req, res) {

    Candidates.find(function (err, allcandidates) {
      if (err)
        res.send(err);
      res.send(JSON.stringify({ status: true, allcandidates }));
    })

  })

//////subject api//////////
router.route('/addsubject').post(function (req, res) {

  var subject = new Subject();
  subject.subject_name = req.body.subjectname;
  subject.save(function (err) {
    if (err) {
      res.send(err);
    }
    else {
      res.send(JSON.stringify({ status: true, message: 'Subject Added successfully' }));
    }

  })
})
  .get(function (req, res) {
    Subject.find(function (err, allsubject) {
      if (err)
        res.send(err);
      res.send(JSON.stringify({ status: true, allsubject }));

    });

  })

/////////////////user type api/////
router.route('/addusertype').post(function (req, res) {
  var usertype = new Usertype();
  usertype.user_type_name = req.body.usertype;
  usertype.save(function (err) {
    if (err) {
      res.send(err);
    }
    else {
      res.send(JSON.stringify({ status: true, message: 'usertype Added successfully' }));
    }

  });

})
  .get(function (req, res) {
    Usertype.find(function (err, usertype) {
      if (err)
        res.send(err);
      res.send(JSON.stringify({ status: true, usertype }));


    })

  });
/////////////////category api////////
router.route('/addcategory').post(function (req, res) {

  var category = new Category();
  category.category_name = req.body.categoryname;
  category.save(function (err) {
    if (err) {
      res.send(err);
    }
    else {
      res.send(JSON.stringify({ status: true, message: 'Category Added successfully' }));
    }
  });
})
  .get(function (req, res) {
    Category.find(function (err, category) {
      if (err)
        res.send(err);
      res.send(JSON.stringify({ status: true, category }));
    })

  })

//////////////////////register api////////
router.route('/register')
  // create contact: POST http://localhost:8080/api/contacts
  .post(function (req, res) {

    var contact = new Contact();
    contact.name = req.body.name;
    contact.designation = req.body.designation;
    contact.password = req.body.password;
    contact.emailid = req.body.emailid;
    contact.mobileno = req.body.mobileno;
    contact.experience = req.body.experience;

    console.log(req.body.name);

    contact.save(function (err) {
      if (err) {
        res.send(err);
      } else {
        res.send(JSON.stringify({ status: true, message: 'Contact Added successfully' }));
      }
    });

  })
  //GET all contacts: http://localhost:8080/api/contacts
  .get(function (req, res) {
    Contact.find(function (err, contacts) {
      if (err)
        res.send(err);
      res.send(JSON.stringify({ status: true, contacts }));
    });
  });
/////////////////////
app.use('/api', router);
app.listen(theport);
